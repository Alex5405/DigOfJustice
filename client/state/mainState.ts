import { makeAutoObservable, runInAction } from "mobx";
import { $api } from "./api";

export interface DataType {
  title: {
    text: string;
    bold: boolean;
  }[];
  place: string[];
  date: string;
  regOpened: boolean;
  techWorks: boolean;
  techWorksHeaderText: string;
  techWorksParagraphText: string;
  confData: string[];
  confTasks: {
    src: string;
    width: number;
    height: number;
    text: string;
  }[];
  support: SupportType[];
  techSupport: SupportType[];
  footerText: string;
}

export interface SupportType {
  img: string;
  firstName: string;
  lastName: string;
  tg: string;
}

class MainState {
  data: DataType | null = null;
  techWorks: boolean = false;
  regOpened: boolean = false;
  isLoading: boolean = true;
  error: boolean = false;
  errorMessage: any = null;
  showData: boolean = false;

  constructor() {
    makeAutoObservable(this);
  }

  setData(data: DataType | null) {
    this.data = data;
  }

  setTechWorks(data: boolean) {
    this.techWorks = data;
  }

  setRegOpened(data: boolean) {
    this.regOpened = data;
  }

  setIsLoading(data: boolean) {
    this.isLoading = data;
  }

  setError(data: boolean) {
    this.error = data;
  }

  setErrorMessage(data: any) {
    this.errorMessage = data;
  }

  setShowData(data: boolean) {
    this.showData = data;
  }

  async getSettings() {
    try {
      let response = await $api.get<DataType>("/settings");
      if (!response.data.techWorks) {
        runInAction(() => {
          this.setData(response.data);
          this.setTechWorks(response.data.techWorks);
          this.setRegOpened(response.data.regOpened);
          this.setShowData(true);
          setTimeout(() => {
            this.setIsLoading(false);
          }, 1500);
        });
      } else {
        runInAction(() => {
          this.setData(response.data);
          this.setTechWorks(response.data.techWorks);
          setTimeout(() => {
            this.setIsLoading(false);
          }, 1500);
        });
      }
    } catch (error) {
      runInAction(() => {
        this.setErrorMessage(error);
        this.setError(true);
        this.setIsLoading(false);
      });
    }
  }
}

export default new MainState();
