import {observer} from "mobx-react-lite";
import mainState from "../../state/mainState";

const Footer = () => {
    return (
        <footer>
            <p>{mainState.data?.footerText}</p>
        </footer>
    );
};

export default observer(Footer);