/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: process.env.API_URL.replace("https://", ""),
        pathname: "/img/**",
      },
      {
        protocol: "https",
        hostname: "api.virusbeats.ru",
        pathname: "/img/**",
      },
    ],
  },
  env: {
    API_URL: process.env.API_URL,
  },
};

module.exports = nextConfig;
