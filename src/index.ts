import AdminConferenceModel from "./database/models/AdminConferenceModel";
import AdminModel from "./database/models/AdminModel";
import AdminTokenModel from "./database/models/AdminTokenModel";
import ConferenceModel from "./database/models/ConferenceModel";
import ProgramModel from "./database/models/ProgramModel";
import ReportModel from "./database/models/ReportModel";
import RoleAdminModel from "./database/models/RoleAdminModel";
import RoleModel from "./database/models/RoleModel";
import SectionButtonModel from "./database/models/SectionButtonModel";
import SectionModel from "./database/models/SectionModel";
import SpeakerModel from "./database/models/SpeakerModel";
import SpeakerProgramModel from "./database/models/SpeakerProgramModel";
import SpeakerSectionModel from "./database/models/SpeakerSectionModel";

//Modules
import cookieParser from "cookie-parser"
import express from "express"
import cors from "cors"
import compression from "compression"


import dotenv from "dotenv"

dotenv.config();

//Variables
import sequelize from "./database/connectDb";
import errorMiddleware from "./middlewares/errorMiddleware"

const pid = process.pid

const app = express();

const PORT = 5000;

/*Whitelist, it limits by domain which sites can send requests */
const whiteList = [process.env.CLIENT_URL, process.env.ADMIN_URL, undefined];

//Image folder
app.use("/img", express.static("./files/images"));
//Compress any requests
app.use(compression());
//Parse JSON
app.use(express.json());
//Working with cookies
app.use(cookieParser());
//Cors with Whitelist
app.use(cors({
	credentials: true, origin: function(origin, callback) {
		if(whiteList.indexOf(origin) !== -1) {
			callback(null, true);
		}
	},
}));

//Routers
import v1Router from "./routers/v1Router";
app.use("/api/v1", v1Router)

app.use(errorMiddleware);

const startServer = async() => {
	try {
		await sequelize.authenticate().then(async() => {
			await sequelize.sync({alter: true});
			await AdminConferenceModel.sync()
			await AdminModel.sync()
			await AdminTokenModel.sync()
			await ConferenceModel.sync()
			await ProgramModel.sync()
			await ReportModel.sync()
			await RoleAdminModel.sync()
			await RoleModel.sync()
			await SectionButtonModel.sync()
			await SectionModel.sync()
			await SpeakerModel.sync()
			await SpeakerSectionModel.sync()
			await SpeakerProgramModel.sync()
			console.log("Connection with Data Base has been established successfully.");
		});
		await app.listen(PORT, () => {
			console.log(`Server started on PORT = ${PORT}, PID: ${pid}`);
		});
	} catch(error) {
		console.log(error);
	}
};

export default app

startServer().then(() => {
	console.log("Server successfully started!")
});