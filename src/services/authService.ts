import AdminModel from "../database/models/AdminModel";
import ApiError from "../exceptions/apiError";
import bcrypt from "bcrypt"
import AdminDto from "../dtos/adminDto";
import TokenService from "./tokenService";

export default class AuthService {
	static async register(login: string, password: string) {
		const candidate = await AdminModel.findOne({where: {login}})
		if(candidate) {
			throw ApiError.BadRequest("Пользователь с таким логином уже существует")
		}
		const hash = await bcrypt.hash(password, 6)
		const admin = await AdminModel.create({login, hash})
		const adminDto = new AdminDto(admin)
		const tokens = await TokenService.generateTokens({...adminDto})
		await TokenService.saveToken(adminDto.id, tokens.refreshToken)
		return {
			...tokens,
			user: {adminDto}
		}
	}
	static async login(login: string, password: string) {
		const admin = await AdminModel.findOne({where: {login}})
		if(!admin) {
			throw ApiError.BadRequest("Пользователь с таким логином не найден")
		}
		const isPassEquals = await bcrypt.compare(password, admin.hash)
		if(!isPassEquals) {
			throw ApiError.BadRequest("Неправильный пароль")
		}
		const adminDto = new AdminDto(admin)
		const tokens = await TokenService.generateTokens({...adminDto})
		await TokenService.saveToken(adminDto.id, tokens.refreshToken)
		return {
			...tokens,
			user: {adminDto}
		}
	}
	static async logout(refreshToken: string) {
		const token = await TokenService.removeToken(refreshToken)
		return token
	}
	static async refresh(refreshToken: string) {
		if(!refreshToken) {
			throw ApiError.UnathorizedError()
		}
		const adminData = await TokenService.validateRefreshToken(refreshToken)
		const tokenFromDB = await TokenService.findToken(refreshToken)
		if(!adminData || !tokenFromDB){
			throw ApiError.UnathorizedError()
		}
		const admin = await AdminModel.findOne({
			where: {id: tokenFromDB.adminId}
		})
		const adminDto = new AdminDto(admin!)
		const tokens = await TokenService.generateTokens({...adminDto})
		await TokenService.saveToken(adminDto.id, tokens.refreshToken)
		return {
			...tokens,
			user: {adminDto}
		}
	}
}