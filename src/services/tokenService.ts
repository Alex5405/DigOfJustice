import jwt from "jsonwebtoken"
import dotenv from "dotenv";
import AdminTokenModel from "../database/models/AdminTokenModel";
import AdminDto from "../dtos/adminDto";

dotenv.config()

export default class TokenService {
	static async generateTokens(payload: AdminDto) {
		const awaiter = Promise.all([
			jwt.sign(payload, process.env.JWT_ACCESS_SECRET!, {expiresIn: "30s",}),
			jwt.sign(payload, process.env.JWT_REFRESH_SECRET!, {expiresIn: "30d",})
		])
		let tokens: {
			accessToken: string,
			refreshToken: string
		} = {accessToken: "", refreshToken: ""};
		await awaiter.then((value) => {
			tokens = {
				accessToken: value[0],
				refreshToken: value[1]
			}
		})
		return tokens
	}

	static async validateAccessToken(token: string) {
		try {
			return jwt.verify(token, process.env.JWT_ACCESS_SECRET!)
		} catch(e) {
			return null
		}
	}

	static async validateRefreshToken(token: string) {
		try {
			return jwt.verify(token, process.env.JWT_REFRESH_SECRET!)
		} catch(e) {
			return null
		}
	}

	static async saveToken(adminId: number, refreshToken: string) {
		const tokenData = await AdminTokenModel.findOne({where: {adminId}})
		if(tokenData) {
			tokenData.refreshToken = refreshToken
			const token = await tokenData.update(
				{
					refreshToken: tokenData.refreshToken
				},
				{where: {adminId}}
			)
			return token
		}
		return await AdminTokenModel.create({refreshToken, adminId})
	}

	static async removeToken(refreshToken: string) {
		return await AdminTokenModel.destroy({where: {refreshToken}})
	}

	static async findToken(refreshToken: string) {
		return await AdminTokenModel.findOne({where: {refreshToken}})
	}
}
