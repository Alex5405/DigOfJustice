import { Router } from "express";
import DebugService from "../services/debugService";
import DebugController from "../controllers/debugController";

const router = Router()

router.post("/registerAdmin", DebugController.registerDefaultAdmin)
router.post("/recreateDataBase", DebugController.recreateDataBase)

export default router