import ApiError from "../exceptions/apiError";
import express from "express";
import TokenService from "../services/tokenService";

export default function(req: express.Request, res: express.Response, next: express.NextFunction) {
	try {
		const authHeader = req.headers.authorization
		if(!authHeader) {
			return next(ApiError.UnathorizedError())
		}
		const accessToken = authHeader.split(" ")[1]
		if(!accessToken){
			return next(ApiError.UnathorizedError())
		}
		const userData = TokenService.validateAccessToken(accessToken)
		if(!userData) {
			return next(ApiError.UnathorizedError())
		}
		next()
	} catch(error) {
		return next(ApiError.UnathorizedError())
	}
}