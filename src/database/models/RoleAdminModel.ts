import { DataTypes, ForeignKey, Model, Optional } from "sequelize";
import sequelize from "../connectDb";

import RoleModel from "./RoleModel";
import AdminModel from "./AdminModel";

interface RoleAdminModelI {
	id: number,
	RoleModelId: ForeignKey<number>,
	AdminModelId: ForeignKey<number>
}

type RoleAdminModelCreation = Optional<RoleAdminModelI, "id">

class RoleAdminModel extends Model<RoleAdminModelI, RoleAdminModelCreation> {
	declare id: number
	declare RoleModelId: ForeignKey<number>
	declare AdminModelId: ForeignKey<number>
}

RoleAdminModel.init({
	id: {
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false,
	}
}, {
	timestamps: false,
	tableName: "RoleAdmin",
	sequelize
})

AdminModel.belongsToMany(RoleModel, {through: RoleAdminModel})
RoleModel.belongsToMany(AdminModel, {through: RoleAdminModel})

export default RoleAdminModel