import { Model, DataTypes, Optional, ForeignKey } from 'sequelize';
import sequelize from "../connectDb";

export interface ReportsModelI {
	id: number,
	fullName: string,
	email: string,
	activityType: string,
	studyPlaceAndSpecialy: string,
	workPlaceAndPosition: string,
	acDegree: string,
	topic: string,
	section: string,
	fullNameSupervisor: string,
	rankSupervisor: string,
	positionSupervisor: string,
	formOfParticipation: string,
	moderated: boolean,
	comand: {
		fullName: string
		description: string
	}[],
	SectionId: ForeignKey<number>;
}

type ReportsModelCreation = Optional<ReportsModelI, "id">

class ReportModel extends Model<ReportsModelI, ReportsModelCreation> {
	declare id: number
	declare fullName: string
	declare email: string
	declare activityType: string
	declare studyPlaceAndSpecialy: string
	declare workPlaceAndPosition: string
	declare acDegree: string
	declare topic: string
	declare section: string
	declare fullNameSupervisor: string
	declare rankSupervisor: string
	declare positionSupervisor: string
	declare formOfParticipation: string
	declare moderated: boolean
	declare comand: {
		fullName: string
		description: string
	}[]
	declare SectionId: ForeignKey<number>
}

ReportModel.init({
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
		},
		fullName: {
			type: DataTypes.STRING(1000),
			allowNull: false,
		},
		email: {
			type: DataTypes.STRING(1000),
			allowNull: false,
		},
		activityType: {
			type: DataTypes.STRING(1000),
			allowNull: false,
		},
		studyPlaceAndSpecialy: {
			type: DataTypes.STRING(1000),
		},
		workPlaceAndPosition: {
			type: DataTypes.STRING(1000),
		},
		acDegree: {
			type: DataTypes.STRING(1000),
			allowNull: false,
		},
		topic: {
			type: DataTypes.STRING(1000),
			allowNull: false,
		},
		comand: {
			type: DataTypes.ARRAY(DataTypes.JSONB)
		},
		section: {
			type: DataTypes.STRING(1000),
			allowNull: false,
		},
		fullNameSupervisor: {
			type: DataTypes.STRING(1000),
			allowNull: false,
		},
		rankSupervisor: {
			type: DataTypes.STRING(1000),
			allowNull: false,
		},
		positionSupervisor: {
			type: DataTypes.STRING(1000),
			allowNull: false,
		},
		formOfParticipation: {
			type: DataTypes.STRING(1000),
			allowNull: false,
		},
		moderated: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},
	},
	{
		sequelize,
		tableName: "Report",
		timestamps: false
	})

export default ReportModel;