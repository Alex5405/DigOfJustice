import { DataTypes, ForeignKey, Model, Optional } from "sequelize";
import sequelize from "../connectDb";

import AdminModel from "./AdminModel";
import ConferenceModel from "./ConferenceModel";

export interface AdminConferenceModelI {
	id: number
	AdminModelId: ForeignKey<number>
	ConferenceModelId: ForeignKey<number>
}

type AdminConferenceModelCreation = Optional<AdminConferenceModelI, "id">

class AdminConferenceModel extends Model<AdminConferenceModelI, AdminConferenceModelCreation> {
	declare id: number
	declare AdminModelId: ForeignKey<number>
	declare ConferenceModelId: ForeignKey<number>
}

AdminConferenceModel.init({
	id: {
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false,
	}
}, {sequelize, tableName: "AdminConference"})

AdminModel.belongsToMany(ConferenceModel, {through: AdminConferenceModel})
ConferenceModel.belongsToMany(AdminModel, {through: AdminConferenceModel})

export default AdminConferenceModel