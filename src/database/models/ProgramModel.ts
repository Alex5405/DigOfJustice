import { Model, DataTypes, Optional, ForeignKey } from 'sequelize';
import sequelize from "../connectDb";

export interface ProgramListModelI {
	id: number,
	title: string,
	whereIs: string,
	date: string,
	time: string,
	info?: boolean
	ConferenceId: ForeignKey<number>
}

type ProgramListModelCreation = Optional<ProgramListModelI, 'id'>

class ProgramModel extends Model<ProgramListModelI, ProgramListModelCreation> {
	declare id: number
	declare title: string
	declare whereIs: string
	declare date: string
	declare time: string
	info?: boolean;
	infoOpened?: boolean;
	inform?: any[];
	declare ConferenceId: ForeignKey<number>
}

ProgramModel.init({
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
		},
		title: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		whereIs: {
			type: DataTypes.STRING,
		},
		date: {
			type: DataTypes.STRING,
		},
		time: {
			type: DataTypes.STRING,
		},
	}
	, {
		sequelize,
		tableName: "Program",
		timestamps: false
	})


export default ProgramModel;