import { DataTypes, ForeignKey, Model, Optional } from "sequelize";
import sequelize from "../connectDb";
import SpeakerModel from "./SpeakerModel";
import ProgramModel from "./ProgramModel";

type SpeakerProgramModelCreation = Optional<SpeakerProgramModelI, "id">

export interface SpeakerProgramModelI {
	id: number
	text: string
	SpeakerModelId: ForeignKey<number>
	ProgramModelId: ForeignKey<number>
}

class SpeakerProgramModel extends Model<SpeakerProgramModelI, SpeakerProgramModelCreation> {
	declare id: number
	declare text: string
	declare SpeakerModelId: ForeignKey<number>
	declare ProgramModelId: ForeignKey<number>
}

SpeakerProgramModel.init({
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
		},
		text: {
			type: DataTypes.STRING(1000),
			allowNull: false
		}
	},
	{
		timestamps: false,
		tableName: "SpeakerProgram",
		sequelize
	})

SpeakerModel.belongsToMany(ProgramModel, {
	through: SpeakerProgramModel
})

ProgramModel.belongsToMany(SpeakerModel, {
	through: SpeakerProgramModel
})

export default SpeakerProgramModel