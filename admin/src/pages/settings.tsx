import MainLayout from "@/layouts/mainLayout";
import '@/styles/Home.module.css'
import RouteGuard from "@/guard/routeGuard";
import Head from "next/head";


export default function Settings() {
	return (
		<RouteGuard>
			<>
				<Head>
					<title>CMR AP | Отправленные доклады</title>
				</Head>
				<MainLayout>
					<div className="w-full h-full bg-cyan-900">

					</div>
				</MainLayout>
			</>
		</RouteGuard>
	)
}