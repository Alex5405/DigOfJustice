import axios from "axios"

export const API_URL = `https://api.virusbeats.ru/api/v1/admin`

export interface LoginI {
	accessToken: string
	refreshToken: string,
	user: {
		adminDto: {
			id: number
			login: string
		}
	}
}

const $api = axios.create({
	withCredentials: true,
	baseURL: API_URL
})

$api.interceptors.request.use((config) => {
	config.headers.Authorization = `Bearer ${localStorage.getItem("accessToken")}`
	return config
})

$api.interceptors.response.use((config) => {
	return config
}, async (error) => {
	const originalRequest = error.config
	if(error.response.status === 401 && error.config && !originalRequest._isRetry) {
		originalRequest._isRetry = true
		try {
			const response = await axios.get<LoginI>(`${API_URL}/refresh`, {withCredentials: true})
			localStorage.setItem('accessToken', response.data.accessToken)
			return $api.request(originalRequest)
		} catch(e) {
			console.log("Не авторизован!")
		}
	}
	throw error;
})

export default $api