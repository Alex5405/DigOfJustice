import { combineReducers } from "redux";
import { configureStore } from "@reduxjs/toolkit";
// import speakersReducer from "./reducers/SpeakerSlice"
import authReducer from "./reducers/AuthSlice"
import reportsReducer from "./reducers/ReportsSlice"

const rootReducer = combineReducers({
	authReducer,
	reportsReducer
})

export const setupStore = () => {
	return configureStore({
		reducer: rootReducer
	})
}

export type RootStateType = ReturnType<typeof rootReducer>
export type AppStoreType = ReturnType<typeof setupStore>
export type AppDispatch = AppStoreType["dispatch"]