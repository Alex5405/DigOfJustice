import { createSlice } from "@reduxjs/toolkit";

interface SpeakerSliceI {
	speakers: SpeakersModelI[],
	isLoading: boolean,
	error: string;
}

const initialState: SpeakerSliceI = {
	speakers: [],
	isLoading: false,
	error: ""
}

export const speakersSlice = createSlice({
	name: "speakersSlice",
	initialState,
	reducers: {

	}
})

export default speakersSlice.reducer;